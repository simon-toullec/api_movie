<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Trait\DefaultFieldTrait;
use App\Entity\Trait\PersonTrait;
use App\Repository\DirectorRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: DirectorRepository::class)]
#[ApiResource()]
class Director
{
    use DefaultFieldTrait;
    use PersonTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: PersonType::class, inversedBy: 'directors')]
    private PersonType $personType;

    #[ORM\OneToMany(mappedBy: 'director', targetEntity: Movie::class, cascade:["persist", "remove"])]
    private Collection $movies;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Assert\Range(minMessage:'Le salaire doit être supérieur à 1267€.', min:1267)]
    private float $salary;

    public function __construct(){
        // $this->createdAt = new DateTime();
        $this->movies = new ArrayCollection();
        $this->id = Uuid::v1();

    }


    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPersonType(): ?PersonType
    {
        return $this->personType;
    }

    public function setPersonType(?PersonType $personType): self
    {
        $this->personType = $personType;

        return $this;
    }

    /**
     * @return Collection<int, Movie>
     */
    public function getMovies(): Collection
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie): self
    {
        if (!$this->movies->contains($movie)) {
            $this->movies[] = $movie;
            $movie->setDirector($this);
        }

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movies->removeElement($movie)) {
            // set the owning side to null (unless already changed)
            if ($movie->getDirector() === $this) {
                $movie->setDirector(null);
            }
        }

        return $this;
    }

    public function getSalary(): ?float
    {
        return $this->salary;
    }

    public function setSalary(?float $salary): self
    {
        $this->salary = $salary;

        return $this;
    }
}
