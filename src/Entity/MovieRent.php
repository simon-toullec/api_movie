<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Trait\DefaultFieldTrait;
use App\Repository\MovieRentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: MovieRentRepository::class)]
class MovieRent
{
    use DefaultFieldTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\Column(type: 'float')]
    private float $amount;

    #[ORM\ManyToOne(targetEntity: Currency::class, inversedBy: 'movieRents')]
    private Currency $currency;

    public function __construct(){
        $this->id = Uuid::v1();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
