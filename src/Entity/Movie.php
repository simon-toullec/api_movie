<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Trait\DefaultFieldTrait;
use App\Repository\MovieRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: MovieRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['movie']])]
class Movie
{

    use DefaultFieldTrait;


    public const NOM="NOM";

    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'datetime')]
    private DateTime $releaseDate;

    #[ORM\ManyToMany(targetEntity: Producer::class, inversedBy: 'movies')]
    private Collection $producers;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'movies')]
    private Collection $users;

    #[ORM\ManyToOne(targetEntity: Director::class, inversedBy: 'movies')]
    /**
     * @Groups({"movie", "export"})
     */
    private Director $director;

    public function __construct(){
        $this->producers = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->id = Uuid::v1();
    }


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getDirector(): ?Director
    {
        return $this->director;
    }

    public function setDirector(?Director $director): self
    {
        $this->director = $director;

        return $this;
    }

        /**
     * @return Collection<int, Producer>
     */
    public function getProducers(): Collection
    {
        return $this->producers;
    }

    public function addProducer(Producer $producer): self
    {
        if (!$this->producers->contains($producer)) {
            $this->producers[] = $producer;
        }

        return $this;
    }

    public function removeProducer(Producer $producer): self
    {
        $this->producers->removeElement($producer);

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }
}
