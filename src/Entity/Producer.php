<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Trait\DefaultFieldTrait;
use App\Entity\Trait\PersonTrait;
use App\Repository\ProducerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: ProducerRepository::class)]
class Producer
{
    use DefaultFieldTrait;

    use PersonTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: PersonType::class, inversedBy: 'producers')]
    private PersonType $personType;

    #[ORM\ManyToMany(targetEntity: Movie::class, mappedBy: 'producers')]
    private Collection $movies;

    public function __construct()
    {
        $this->movieProducers = new ArrayCollection();
        $this->movies = new ArrayCollection();
        $this->id = Uuid::v1();

    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPersonType(): ?PersonType
    {
        return $this->personType;
    }

    public function setPersonType(?PersonType $personType): self
    {
        $this->personType = $personType;

        return $this;
    }

    /**
     * @return Collection<int, Movie>
     */
    public function getMovies(): Collection
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie): self
    {
        if (!$this->movies->contains($movie)) {
            $this->movies[] = $movie;
            $movie->addProducer($this);
        }

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movies->removeElement($movie)) {
            $movie->removeProducer($this);
        }

        return $this;
    }
}
