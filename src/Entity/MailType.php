<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Trait\DefaultFieldTrait;
use App\Repository\MailTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: MailTypeRepository::class)]
class MailType
{
    use DefaultFieldTrait;


    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $wording;

    
    #[ORM\Column(type: 'string', length: 255)]
    private string $reference;


    public function __construct(){
        $this->id = Uuid::v1();

    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWording(): ?string
    {
        return $this->wording;
    }

    public function setWording(string $wording): self
    {
        $this->wording = $wording;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }
}
