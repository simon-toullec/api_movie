<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Trait\DefaultFieldTrait;
use App\Repository\CurrencyRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;


#[ORM\Entity(repositoryClass: CurrencyRepository::class)]
class Currency
{

    use DefaultFieldTrait;

    
    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $wordingShort;

    #[ORM\Column(type: 'string', length: 255)]
    private string $wordingLong;

    #[ORM\Column(type: 'string', length: 255)]
    private string $reference;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: MovieRent::class)]
    private Collection $movieRents;

    public function __construct()
    {
        $this->movieRents = new ArrayCollection();
        $this->id = Uuid::v1();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWordingShort(): ?string
    {
        return $this->wordingShort;
    }

    public function setWordingShort(string $wordingShort): self
    {
        $this->wordingShort = $wordingShort;

        return $this;
    }

    public function getWordingLong(): ?string
    {
        return $this->wordingLong;
    }

    public function setWordingLong(string $wordingLong): self
    {
        $this->wordingLong = $wordingLong;

        return $this;
    }
    
    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection<int, MovieRent>
     */
    public function getMovieRents(): Collection
    {
        return $this->movieRents;
    }

    public function addMovieRent(MovieRent $movieRent): self
    {
        if (!$this->movieRents->contains($movieRent)) {
            $this->movieRents[] = $movieRent;
            $movieRent->setCurrency($this);
        }

        return $this;
    }

    public function removeMovieRent(MovieRent $movieRent): self
    {
        if ($this->movieRents->removeElement($movieRent)) {
            // set the owning side to null (unless already changed)
            if ($movieRent->getCurrency() === $this) {
                $movieRent->setCurrency(null);
            }
        }

        return $this;
    }
}
