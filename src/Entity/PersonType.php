<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Trait\DefaultFieldTrait;
use App\Repository\PersonTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: PersonTypeRepository::class)]
class PersonType
{
    use DefaultFieldTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'string', unique: true)]
    #[ApiProperty(identifier: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $wording;

    #[ORM\Column(type: 'string', length: 255)]
    private string $reference;

    #[ORM\OneToMany(mappedBy: 'personType', targetEntity: Director::class)]
    private Collection $directors;

    #[ORM\OneToMany(mappedBy: 'personType', targetEntity: Producer::class)]
    private Collection $producers;

    public function __construct()
    {
        $this->directors = new ArrayCollection();
        $this->producers = new ArrayCollection();
        $this->id = Uuid::v1();

    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWording(): ?string
    {
        return $this->wording;
    }

    public function setWording(string $wording): self
    {
        $this->wording = $wording;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return Collection<int, Director>
     */
    public function getDirectors(): Collection
    {
        return $this->directors;
    }

    public function addDirector(Director $director): self
    {
        if (!$this->directors->contains($director)) {
            $this->directors[] = $director;
            $director->setPersonType($this);
        }

        return $this;
    }

    public function removeDirector(Director $director): self
    {
        if ($this->directors->removeElement($director)) {
            // set the owning side to null (unless already changed)
            if ($director->getPersonType() === $this) {
                $director->setPersonType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Producer>
     */
    public function getProducers(): Collection
    {
        return $this->producers;
    }

    public function addProducer(Producer $producer): self
    {
        if (!$this->producers->contains($producer)) {
            $this->producers[] = $producer;
            $producer->setPersonType($this);
        }

        return $this;
    }

    public function removeProducer(Producer $producer): self
    {
        if ($this->producers->removeElement($producer)) {
            // set the owning side to null (unless already changed)
            if ($producer->getPersonType() === $this) {
                $producer->setPersonType(null);
            }
        }

        return $this;
    }
}
