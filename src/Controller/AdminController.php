<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/admin","admin_")]
class AdminController extends AbstractController
{
    #[Route("/","index")]
    public function index(): JsonResponse
    {
        return new JsonResponse([
            "success" => true
        ]);
    }
}
