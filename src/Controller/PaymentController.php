<?php

namespace App\Controller;

use App\Service\StripeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/payment', "payment_")]
class PaymentController extends AbstractController{

    private StripeService $stripeService;

    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    #[Route('/add', name:"add")]
    public function addPayment(
        Request $request
    ): JsonResponse
    {

        $this->stripeService->init();

        return new JsonResponse([
            "success" => true
        ]);
    }
}