<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(
        Request $request, 
        SerializerInterface $serializer,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $em,
        ValidatorInterface $validator
        ): JsonResponse
    {
        try{
            $user = $serializer->deserialize($request->getContent(), User::class, 'json');

            $user->setPassword(
                $userPasswordHasher->hashPassword(
                        $user,
                        $user->getPassword()
                    )
                );
            $errors = $validator->validate($user);

            if (count($errors) > 0) {
                $errorsString = (string) $errors;

                return $this->json([
                    'errors' => $errorsString
                ]);
            }

        }catch(NotEncodableValueException $e){
            return $this->json([
                'status' => 400,
                'message' =>$e->getMessage(),
            ]);
        }

        $em->persist($user);
        $em->flush();

        return $this->json($user,201);
    }
}
