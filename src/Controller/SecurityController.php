<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{

    #[Route('/api/login', name:"api_login")]
    public function login():JsonResponse
    {
        dd($this->getUser());

        return new JsonResponse([
            "user" => $this->getUser() ? $this->getUser()->getId() : null
        ]);
    }

    #[Route('/infos', name:"api_infos")]
    public function infos():JsonResponse
    {


        return new JsonResponse([
            "nom"=> "nom",
            "prenom"=>"prenom",
            "email"=>"email",
            "roles"=>"ROLE_ADMIN",
        ]);
    }

}