<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Repository\MovieRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route("/movie", "movie_")]
class MovieController extends AbstractController
{

    
    #[Route('/export-csv', name: 'export_csv')] 
    public function exportCsv(
        MovieRepository $movieRepository,
        SerializerInterface $serializer
    ): JsonResponse
    {
        $listMovie = $movieRepository->findAll();
        $list =[];
        $listResult = [];
        $listUnSet = [];

        $j = 0;

        foreach($listMovie as $value){
            // object to array [value,value, ...]
            $list[] = $serializer->serialize(
                $value,
                'csv',
                ['groups' => 'export']
            );


            $listUnSet[$j] = explode("\n",$list[$j]);//separate colum and row in each array

            //delete array which no utility
            if($j !== 0){
                unset($listUnSet[$j][0]);
            }elseif($j === 0){
                $listUnSet[$j][0] =  str_replace('"','',$listUnSet[$j][0]);
                unset($listUnSet[$j][2]);
            }

            if(count($listUnSet[$j]) === 2){
                unset($listUnSet[$j][2]);
            }

            $j++;
        }

        $z= 0;

        for($m = 0; $m < count($listUnSet); $m++){//format array for csv file

            foreach($listUnSet[$m] as $value){
                $listResult[$z] = $value;
                $listResult[$z] = explode(',',$listResult[$z]);
                
                $z++;
                
            }
        }
    

        $fp = fopen('upload/movie/export.csv', 'w');
        
        foreach ($listResult as $fields) {
            fputcsv($fp, $fields,";");
        }
        
        fclose($fp);


        return new JsonResponse([
            'success' => true
        ]);
    }

    #[Route('/import-csv', name: 'import_csv')] 
    public function importCsv(): JsonResponse
    {
        $targetPath = "upload/movie/".basename($_FILES['inpFile']['name']);
        move_uploaded_file($_FILES['inpFile']['tmp_name'],$targetPath);

        $index = 0;
        $arrColumn = null;
        $arrayResult = [];
        $member = null;
        
        if (($fp = fopen($targetPath, "r")) !== FALSE) {

            while (($row = fgetcsv($fp, 1000, ",")) !== FALSE) {

                if($index === 0){
                    $arrColumn = explode(';',$row[0]);
                }else{
                    $arrRow = explode(';',$row[0]);

                    for($c = 0; $c < count($arrRow); $c++){
                        $arrayResult[$index][$arrColumn[$c]] = $arrRow[$c];
                    }
                }

                $index++;
            }
            fclose($fp);
        }

        for($j = 1; $j < count($arrayResult); $j++){
            $movie = new Movie();

            foreach($arrayResult[$j] as $key => $value){

                if($key === Movie::NOM){
                    $movie->setName($value);

                }
                
                // else if($key === Movie::PRENOM){
                //     $movie->setFirstName($value);

                // }else if($key === Movie::MF){

                //     if($value === "M"){
                //         $movie->setSex("Homme");

                //     }else{
                //         $movie->setSex("Femme");
            
                //     }

                // }else if($key === Movie::NEE_LE){
                //     $date = DateTime::createFromFormat('d/m/Y', $value);
                //     $strDate = $date->format('Y-m-d');
                //     $resDate = new DateTime($strDate);
                //     $movie->setBirthdate($resDate);

                // }else if($key === Movie::ADRESSE){
                //     $movie->setStreetAddress($value);

                // }
            }

            $this->entityManager->persist($movie);

        }
        

        if($movie){
            $this->entityManager->flush();
        }

        return new JsonResponse([
            'success' => true
        ]);
    }

}
