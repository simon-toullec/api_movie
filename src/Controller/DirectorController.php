<?php

namespace App\Controller;

use App\Entity\Director;
use App\Repository\DirectorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[Route("/director","director_")]
class DirectorController extends AbstractController
{

    #[Route('/show', name: 'show')]
    public function show(
        SerializerInterface $serializer,
        DirectorRepository $directorRepository
        ): JsonResponse
    {

        return $this->json($directorRepository->findAll());
    }

    #[Route('/create','create')]
    public function create(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        ValidatorInterface $validator
        ): JsonResponse
    {

        try{
            $director = $serializer->deserialize($request->getContent(), Director::class, 'json');

            $errors = $validator->validate($director);

            if (count($errors) > 0) {
                $errorsString = (string) $errors;

                return $this->json([
                    'errors' => $errorsString
                ]);
            }

        }catch(NotEncodableValueException $e){
            return $this->json([
                'status' => 400,
                'message' =>$e->getMessage(),
            ]);
        }

        $em->persist($director);
        $em->flush();

        return $this->json($director,201);
    }
}
    