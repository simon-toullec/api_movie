<?php

namespace App\Entity\Trait;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

trait DefaultFieldTrait
{
    #[ORM\Column(type: 'datetime', nullable: true)]
    private Datetime $createdAt;

    #[ORM\Column(type: 'datetime', nullable:true)]
    private Datetime $updatedAt;
    
    #[ORM\Column(type: 'datetime', nullable:true)]
    private Datetime $deletedAt;

    #[ORM\Column(type: 'boolean', nullable:false)]
    private bool $active = true;

    public function __construct(){
        $this->createdAt = new DateTime();
        $this->active = true;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeleteAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function isActive(bool $active)
    {
        return $active;
    }

    public function setIsActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
