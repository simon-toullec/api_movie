<?php

namespace App\Service;


use Stripe\Checkout\Session;
use Stripe\Stripe;
use Stripe\StripeClient;

class StripeService{

    const YOUR_DOMAIN = 'http://localhost:8000/public';
    // const YOUR_DOMAIN = 'http://localhost:4242/public';

    public function init()
    {
        Stripe::setApiKey('sk_test_51LG29iHWlfCeI1sMnJ71YLbDUDg6tLOIJdhyFvHwqXpyQOd0loPaJPQ81Ulg7twceHBjo0izSPEqqFaAZ1mL7XAu001FUdho3g');        
        $stripe = new StripeClient('sk_test_51LG29iHWlfCeI1sMnJ71YLbDUDg6tLOIJdhyFvHwqXpyQOd0loPaJPQ81Ulg7twceHBjo0izSPEqqFaAZ1mL7XAu001FUdho3g');

        $product = $stripe->products->create(
            [
                'name' => 'Basic Dashboard',
                'default_price_data' => [
                    'unit_amount' => 1000,
                    'currency' => 'usd',
                    'recurring' => ['interval' => 'month'],
                ],
                'expand' => ['default_price'],
            ]
        );

        $price = $stripe->prices->create(
            [
                'product' => $product['id'],
                'unit_amount' => 1000,
                'currency' => 'usd',
                // 'recurring' => ['interval' => 'month'],
            ]
        );
        
        $checkout_session = Session::create([
            'line_items' => [[
            # Provide the exact Price ID (e.g. pr_1234) of the product you want to sell
            'price' => $price['id'],
            'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => self::YOUR_DOMAIN . '/success.html',
            'cancel_url' => self::YOUR_DOMAIN . '/cancel.html',
        ]);
    
    }
}